package com.techuniversity.demospringbootinitz.controlador;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoControlador {

    @RequestMapping("/")
    public String index (){
        return "¡Saludos desde Spring Boot!";
    }
}
