package com.techuniversity.demospringbootinitz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringbootInitzApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringbootInitzApplication.class, args);
	}

}
